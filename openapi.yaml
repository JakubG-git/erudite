openapi: 3.0.0
info:
    title: Erudite - Library Management System API
    version: 0.0.1
paths:
  /editions:
    post:
      tags:
        - edition
      summary: Add edition to database
      description: ''
      operationId: addEdition
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Edition'
      responses:
        '200':
          description: Operation was successful
        '400':
          description: Invalid input
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
      security:
        - basicAuth: []
  /editions/search:
    get:
      tags:
        - edition
      summary: Search for an edition
      description: ''
      operationId: searchEdition
      parameters:
        - in: query
          name: title
          schema:
            type: string
        - in: query
          name: author
          schema:
            type: string
        - in: query
          name: tag
          schema:
            type: string
        - in: query
          name: isbn
          schema:
            type: string
        - in: query
          name: publisher
          schema:
            type: string
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                type: array
                items: 
                  $ref: '#/components/schemas/Edition'
        '404':
          description: Could not find any edition with provided parameters

  /editions/{id}:
    get:
      tags:
        - edition
      summary: Get edition with provided id
      description: Returns a single edition
      operationId: getEditionId
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Edition'
        '400':
          description: Invalid id supplied
        '404':
          description: Edition with provided id not found
    put:
      tags:
        - edition
      summary: Update edition
      description: ''
      operationId: updateEditionId
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Edition'
        '400':
          description: Invalid input
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
        '404':
          description: Edition with provided id not found
      security:
        - basicAuth: []
    delete:
      tags:
        - edition
      summary: Delete edition from database
      description: Delete edition from database
      operationId: deleteEditionId
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '204':
          description: Operation was successful
        '400':
          description: Invalid id supplied
        '403':
          description: User not authorized to use this action
      security:
        - basicAuth: []
        
  /books:
    post:
      tags:
        - book
      summary: Add a new book
      description: ''
      operationId: addBook
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Book'
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
      security:
        - basicAuth: []
  /books/{id}:
    get:
      tags:
        - book
      summary: Get a book by ID
      description: ''
      operationId: getBookById
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
        '400':
          description: Invalid id supplied
        '404':
          description: Book with provided id not found
    put:
      tags:
        - book
      summary: Update a book by ID
      description: ''
      operationId: updateBookById
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Book'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
      security:
        - basicAuth: []
    delete:
      tags:
        - book
      summary: Delete a book by ID
      operationId: deleteBookById
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
      responses:
        '204':
          description: Operation was successful
        '404':
          description: Book with provided id not found
      security:
        - basicAuth: []
  /authors:
    post:
      tags:
        - author
      summary: Add author to database
      description: ''
      operationId: addAuthor
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Author'
      responses:
        '200':
          description: Operation was successful
        '400':
          description: Invalid input
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
      security:
        - basicAuth: []
  /author/search:
    get:
      tags:
        - author
      summary: Search for an author
      description: ''
      operationId: Search for an author
      parameters:
        - in: query
          name: name
          schema:
            type: string
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                type: array
                items: 
                  $ref: '#/components/schemas/Author'
        '404':
          description: Author not found for given parameters
  /authors/{id}:
    get:
      tags:
        - author
      summary: Get author with provided id
      description: ''
      operationId: getAuthor
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Author'
        '404':
          description: Author with provided id not found
    put:
      tags:
        - author
      summary: Update author
      description: ''
      operationId: updateAuthor
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Author'
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
        '404':
          description: Author with provided id not found
      security:
        - basicAuth: []
    delete:
      tags:
        - author
      summary: Delete author from database
      description: ''
      operationId: deleteAuthor
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '204':
          description: Operation was successful
        '403':
          description: User not authorized to use this action
        '404':
          description: Author with provided id not found
      security:
        - basicAuth: []

  /tags:
    post:
      tags:
        - tag
      summary: Add tag to database
      description: ''
      operationId: addTag
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Tag'
      responses:
        '200':
          description: Operation was successful
        '400':
          description: Invalid input
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
      security:
        - basicAuth: []
    get:
      tags:
        - tag
      summary: Get all tags from database
      description: ''
      operationId: getAllTags
      parameters:
        - in: query
          name: name
          schema:
            type: string
        - in: query
          name: color
          schema:
            type: string
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Tag'
      security:
        - basicAuth: []
  /tags/{id}:
    get:
      tags:
        - tag
      summary: Get tag with provided id
      description: ''
      operationId: getTag
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Tag'
        '404':
          description: Tag with provided id not found
    put:
      tags:
        - tag
      summary: Update tag
      description: ''
      operationId: updateTag
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Tag'
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
        '404':
          description: Tag with provided id not found
      security:
        - basicAuth: []
    delete:
      tags:
        - tag
      summary: Delete tag from database
      description: ''
      operationId: deleteTag
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '204':
          description: Operation was successful
        '403':
          description: User not authorized to use this action
        '404':
          description: Tag with provided id not found
      security:
        - basicAuth: []

  /catalog:
    post:
      tags:
        - catalog
      summary: Add book to catalog
      description: ''
      operationId: addCatalogComponent
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Catalog'
      responses:
        '200':
          description: Operation was successful
        '400':
          description: Invalid input
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
      security:
        - basicAuth: []
  /catalog/{id}:
    get:
      tags:
        - catalog
      summary: Get component of catalog with provided id
      description: ''
      operationId: getCatalogComponent
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Catalog'
        '404':
          description: Catalog component with provided id not found
    put:
      tags:
        - catalog
      summary: Update catalog component
      description: ''
      operationId: updateCatalogComponent
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Catalog'
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
        '404':
          description: Catalog component with provided id not found
      security:
        - basicAuth: []
    delete:
      tags:
        - catalog
      summary: Delete catalog component from database
      description: ''
      operationId: deleteCatalogComponent
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '204':
          description: Operation was successful
        '403':
          description: User not authorized to use this action
        '404':
          description: Catalog component with provided id not found
      security:
        - basicAuth: []

  /reviews:
    post:
      tags:
        - review
      summary: Add review to database
      description: ''
      operationId: addReview
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Review'
      responses:
        '200':
          description: Operation was successful
        '400':
          description: Invalid input
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
      security:
        - basicAuth: []
  /reviews/getForBook/{id_book}:
    get:
      tags:
        - review
      summary: Get all reviews for given book
      description: ''
      operationId: getReviewsByBook
      parameters:
        - in: path
          name: id_book
          required: true
          schema:
            $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Review'
        '400':
          description: Invalid book id
        '404':
          description: Reviews for given book not found

  /reviews/{id}:
    get:
      tags:
        - review
      summary: Get review with provided id
      description: ''
      operationId: getReview
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Review'
        '404':
          description: Review with provided id not found
    put:
      tags:
        - review
      summary: Update review
      description: ''
      operationId: updateReview
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Review'
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
        '404':
          description: Review with provided id not found
      security:
        - basicAuth: []
    delete:
      tags:
        - review
      summary: Delete review from database
      description: ''
      operationId: deleteReview
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
        '403':
          description: User not authorized to use this action
        '404':
          description: Review with provided id not found
      security:
        - basicAuth: []
    

  /reservations:
    post:
      tags:
        - reservation
      summary: Add reservation to database
      description: ''
      operationId: addReservation
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Reservation'
      responses:       
        '200':
          description: Operation was successful
        '400':
          description: Invalid input
        '401':
          description: User not authenticated content
        '403':
          description: User not authorized to use this action
  /reservations/getForUser/{id_user}:
    get:
      tags:
        - reservation
      summary: Get reservations for given user
      description: ''
      operationId: getReservationsForUsers
      parameters:
        - in: path
          name: id_user
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Reservation'
        '400':
          description: Invalid user id
        '401':
          description: Not authenticated
        '403':
          description: Not authorized for this operation
        '404':
          description: Loan for given user not found
        
    
  /reservations/{id}:
    get:
      tags:
        - reservation
      summary: Get reservation by ID
      description: ''
      operationId: getReservation
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Reservation'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Reservation'
        '404':
          description: Reservation with provided id not found
    put:
      tags:
        - reservation
      summary: Update reservation
      description: ''
      operationId: updateReservation
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Reservation'
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
        '404':
          description: Reservation with provided id not found
      security:
        - basicAuth: []
    delete:
      tags:
        - reservation
      summary: Delete reservation from database
      description: ''
      operationId: deleteReservation
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '204':
          description: Operation was successful
        '403':
          description: User not authorized to use this action
        '404':
          description: Reservation with provided id not found
      security:
        - basicAuth: []
      
  /publishers:
    post:
      tags:
        - publisher
      summary: Add publisher to database
      description: ''
      operationId: addPublisher
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Publisher'
      responses:
        '200':
          description: Operation was successful
        '400':
          description: Invalid input
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
      security:
        - basicAuth: []
  /publishers/search:
    get:
      tags:
        - publisher
      summary: Search for an publisher
      description: ''
      operationId: searchPublisher
      parameters:
        - in: query
          name: name
          schema:
            type: string
        - in: query
          name: country
          schema:
            type: string
        - in: query
          name: description
          schema:
            type: string
        - in: query
          name: logo
          schema:
            type: string
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Publisher'
        '404':
          description: Could not find any publisher with provided parameters
  /publishers/{id}:
    get:
      tags:
      - publisher
      description: ''
      operationId: getPublisher
      summary: Get publisher by ID
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Publisher'
        '404':
          description: Publisher with provided id not found
    put:
      tags:
        - publisher
      summary: Update publisher
      description: ''
      operationId: updatePublisher
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Publisher'
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
        '404':
          description: Publisher with provided id not found
      security:
        - basicAuth: []
    delete:
      tags:
        - publisher
      summary: Delete publisher from database
      description: ''
      operationId: deletePublisher
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '204':
          description: Operation was successful
        '403':
          description: User not authorized to use this action
        '404':
          description: Publisher with provided id not found
      security:
        - basicAuth: []

  /loans:
    post:
      tags:
        - loan
      summary: Add loan to database
      description: ''
      operationId: addLoan
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Loan'
      responses:
        '200':
          description: Operation was successful
        '400':
          description: Invalid input
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
      security:
        - basicAuth: []
  /loans/getForUser/{id_user}:
    get:
      tags:
        - loan
      summary: Get loan for provided user
      description: ''
      operationId: getLoanFOrUser
      parameters:
        - in: path
          required: true
          name: id_user
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Loan'
        '400':
          description: Invalid user id
        '401':
          description: Not authenticated
        '403':
          description: Not authorized for this operation
        '404':
          description: Loan for given user not found
  /loans/{id}:
    get:
      tags:
        - loan
      summary: Get loan with provided id
      description: ''
      operationId: getLoan
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Loan'
        '404':
          description: Loan with provided id not found
    put:
      tags:
        - loan
      summary: Update loan
      description: ''
      operationId: updateLoan
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Loan'
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
        '404':
          description: Loan with provided id not found
      security:
        - basicAuth: []
    delete:
      tags:
        - loan
      summary: Delete loan from database
      description: ''
      operationId: deleteLoan
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '204':
          description: Operation was successful
        '403':
          description: User not authorized to use this action
        '404':
          description: Loan with provided id not found
      security:
        - basicAuth: []
  /users:
    post:
      tags:
        - user
      summary: Add User to database
      description: ''
      operationId: addUser
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
      responses:
        '200':
          description: Operation was successful
        '400':
          description: Invalid input
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
      security:
        - basicAuth: []
  /users/search:
    get:
      tags:
        - user
      summary: Search for an user
      description: ''
      operationId: searchUser
      parameters:
        - in: query
          name: name
          schema:
            type: string
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/User'
        '404':
          description: Could not find any user with provided name
  /users/{id}:
    get:
      tags:
        - user
      summary: Get user with provided id
      description: ''
      operationId: getUser
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '404':
          description: Loan with provided id not found
    put:
      tags:
        - user
      summary: Update user
      description: ''
      operationId: updateUser
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '200':
          description: Operation was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '401':
          description: User not authenticated
        '403':
          description: User not authorized to use this action
        '404':
          description: User with provided id not found
      security:
        - basicAuth: []
    delete:
      tags:
        - user
      summary: Delete user from database
      description: ''
      operationId: deleteUser
      parameters:
        - $ref: '#/components/parameters/Id'
      responses:
        '204':
          description: Operation was successful
        '403':
          description: User not authorized to use this action
        '404':
          description: User with provided id not found
      security:
        - basicAuth: []

components:
  schemas:
    Book:
      required:
        - id
        - title
      properties:
        id:
          type: integer
          format: int64
          example: 13
        title:
          type: string
        year:
          type: integer
          format: int64
          example: 2019
    Edition:
      required:
        - id
        - id_book
        - isbn
      properties:
        id:
          type: integer
          format: int64
          example: 10
        id_book:
          type: integer
          format: int64
          example: 13
        isbn:
          type: string
        pages:
          type: integer
          format: int64
          example: 365
        year:
          type: integer
          format: int64
          example: 2019
        id_publisher:
          type: integer
          format: int64
          example: 22
        description:
          type: string
        cover:
          type: string
    Author:
      required:
        - id
        - name
      properties:
        id:
          type: integer
          format: int64
          example: 10
        name:
          type: string
    Tag:
      required:
        - id
        - name
        - color
      properties:
        id:
          type: integer
          format: int64
          example: 10
        name:
          type: string
        color:
          type: string
    Catalog:
      required:
        - id
        - id_edition
        - shelf_mark
      properties:
        id:
          type: integer
          format: int64
        id_edition:
          type: integer
          format: int64
        shelf_mark:
          type: string
    Review:
      required:
        - id
        - id_edition
        - id_user
        - score
        - timestamp
        - content
      properties:
        id:
          type: integer
          format: int64
        id_edition:
          type: integer
          format: int64
        id_user:
          type: integer
          format: int64
        content:
          type: string
        score:
          type: integer
          format: int64
        timestamp:
          type: string
          format: date-time
          example: 1982-10-10 10:16 AM

    Publisher:
      required:
        - id
        - name
      properties:
        id:
          type: integer
          format: int64
          example: 10
        name:
          type: string
        country:
          type: string
        description:
          type: string
        logo:
          type: string
    
    Reservation:
      required:
        - id
        - id_user
        - id_edition
        - submitted
      properties:
        id:
          type: integer
          format: int64
          example: 10
        id_user:
          type: integer
          format: int64
          example: 13
        id_edition:
          type: integer
          format: int64
          example: 13
        submitted:
          type: string
          format: date
        recipt_term:
          type: string
          format: date
          example: 2022-12-12
    Loan:
      required:
        - id
        - id_user
        - id_catalog
        - loanDate
      properties:
        id:
          type: integer
          format: int64
          example: 10
        id_user:
          type: integer
          format: int64
          example: 13
        id_catalog:
          type: integer
          format: int64
          example: 13
        loanDate:
          type: string
          format: date
        returnDate:
          type: string
          format: date
        prolongation:
          type: integer
          format: int64
          example: 2
    User:
      required:
        - id
        - name
        - password
        - email
      properties:
        id:
          type: integer
          format: int64
          example: 10
        name:
          type: string
        password:
          type: string
          format: password
        email:
          type: string
        birth_date:
          type: string
          format: date
        avatar:
          type: string
        description:
          type: string
  parameters:
    Id:
      name: id
      in: path
      description: id of entity
      required: true
      schema:
        type: integer
        format: int64
  securitySchemes:
    basicAuth:
      type: http
      scheme: basic