package pl.edu.agh.kis.erudite.author;

import org.junit.jupiter.api.Disabled;
import org.springframework.http.MediaType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TestAuthorController {
    @Autowired
    MockMvc mvc;

    @MockBean
    AuthorService service;

    /* Happy tests */

    @Test
    void testGettingOne() throws Exception {
        Author dostoevsky = new Author("Fyodor Dostoevsky");
        when(service.getAuthor(any())).thenReturn(dostoevsky);
        mvc.perform(get("/authors/1"))
                .andExpect(status().isOk()) //200
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.name").value("Fyodor Dostoevsky"));
    }

    /* Unauthenticated tests */

    @Test
    void testAddingWithoutAuthentication() throws Exception {
        mvc.perform(post("/authors"))
                .andExpect(status().isUnauthorized()); // 401
    }

    @Test
    void testDeletingWithoutAuthenticating() throws Exception {
        mvc.perform(delete("/authors/1"))
                .andExpect(status().isUnauthorized()); // 401
    }

    /* Unauthorized tests */
    @Test
    void testAddingWithoutAuthorization() throws Exception {
        mvc.perform(post("/authors")
                        .with(httpBasic("j.nowak@gmail.com", "tajnehaslo"))
                        .with(user("j.nowak@gmail.com").password("tajnehaslo").roles("library_user")))
                .andExpect(status().isForbidden());
    }

    @Test
    void testDeletingWithoutAuthorization() throws Exception {
        mvc.perform(delete("/authors")
                        .with(httpBasic("j.nowak@gmail.com", "tajnehaslo"))
                        .with(user("j.nowak@gmail.com").password("tajnehaslo").roles("library_user")))
                .andExpect(status().isForbidden());
    }

    /* Authorized, but invalid tests */
   @Test
   @Disabled
   void testAddingInvalidAuthor() throws Exception {
       String json = "{\"namei\": \"Jakub\"}";
        mvc.perform(post("/authors")
                .with(httpBasic("j.nowak@gmail.com", "tajnehaslo"))
                .with(user("j.nowak@gmail.com").password("tajnehaslo").roles("librarian"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
            .andExpect(status().isBadRequest());
   }

   /* Authorized with valid content tests */
    @Test
    void testAddingValidAuthor() throws Exception {
        String json = "{\"name\": \"Jakub\"}";
        String username = "j.nowak@gmail.com";
        String password = "tajnehaslo";
        mvc.perform(post("/authors")
                        .with(httpBasic(username, password))
                        .with(user(username).password(password).roles("librarian"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void testValidDelete() throws Exception {
        String username = "j.nowak@gmail.com";
        String password = "tajnehaslo";
        mvc.perform(delete("/authors/1")
                        .with(httpBasic(username, password))
                        .with(user(username).password(password).roles("librarian")))
                .andExpect(status().isOk());
    }
}
