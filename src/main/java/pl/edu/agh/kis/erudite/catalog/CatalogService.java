package pl.edu.agh.kis.erudite.catalog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.kis.erudite.exception.ItemNotFoundException;

@Service
public class CatalogService {
    private final CatalogRepository catalogRepository;

    @Autowired
    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    public Iterable<Catalog> getCatalog() {
        return catalogRepository.findAll();
    }

    public Catalog getCatalogComponent(Long id) {
        return catalogRepository.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
    }

    public void addNewCatalogComponent(Catalog catalog) {
        catalogRepository.save(catalog);
    }

    public void deleteCatalogComponent(Long catalogId) {
        boolean exists = catalogRepository.existsById(catalogId);
        if (!exists) {
            throw new ItemNotFoundException(catalogId);
        }
        catalogRepository.deleteById(catalogId);
    }
}
