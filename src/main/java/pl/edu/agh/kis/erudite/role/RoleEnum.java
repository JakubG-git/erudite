package pl.edu.agh.kis.erudite.role;

public enum RoleEnum {
    library_user, admin, librarian
}
