package pl.edu.agh.kis.erudite.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.kis.erudite.exception.ItemNotFoundException;

@Service
public class PublisherService {
    private final PublisherRepository publisherRepository;

    @Autowired
    public PublisherService(PublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }

    public Iterable<Publisher> getAllPublishers() {
        return publisherRepository.findAll();
    }

    public Publisher getPublisher(Long id) {
        return publisherRepository.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
    }

    public void createPublisher(Publisher publisher) {
        publisherRepository.save(publisher);
    }

    public void deletePublisher(Long id) {
        if(!publisherRepository.existsById(id)) {
            throw new ItemNotFoundException(id);
        }
        publisherRepository.deleteById(id);
    }
}
