package pl.edu.agh.kis.erudite.reservation;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reservations")
@CrossOrigin(origins = "http://localhost:3000")
public class ReservationController {
    private ReservationService service;

    @GetMapping("/{id}")
    public Reservation get(@PathVariable("id") Long id) {
        return service.getReservation(id);
    }
}
