package pl.edu.agh.kis.erudite.reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.kis.erudite.exception.ItemNotFoundException;

@Service
public class ReservationService {
    private ReservationRepository repository;
    @Autowired
    public ReservationService(ReservationRepository repository) {
        this.repository = repository;
    }
    public Reservation getReservation(Long id) {
        return repository.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
    }
}
