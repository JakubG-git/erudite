package pl.edu.agh.kis.erudite.author;

import com.sun.istack.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.edu.agh.kis.erudite.edition.Edition;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table
@EqualsAndHashCode
@NoArgsConstructor
@Setter
@Getter
public class Author {
    @Column(name = "id_author")
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    public Author(String name) {
        this.name = name;
    }
}
